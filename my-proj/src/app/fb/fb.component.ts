import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-fb',
  templateUrl: './fb.component.html',
  styleUrls: ['./fb.component.css']
})
export class FbComponent implements OnInit 
{
  @Input() id = 'mickeymouse'
  fbidUrl = 'https://graph.facebook.com/mickeymouse/picture?type=normal'

  @Output() myEvent = new EventEmitter()

  resolveImageURL(event)
  {
    // creating a new url
    this.fbidUrl=`https://graph.facebook.com/${this.id}/picture?type=normal`
    // choosing to emit a new event
    this.myEvent.emit({value:this.fbidUrl})
  }

  
  constructor() 
  { 

  }

  ngOnInit() 
  {

  }

}
