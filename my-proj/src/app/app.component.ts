import { Component } from '@angular/core';

@Component
({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

// model
export class AppComponent 
{
  // declaring models for data binding - one way binding
  title ='Matt is a noot noot and grant cant play archer but takes the archer class';
  today = new Date();
  myURL = 'https://i.pinimg.com/originals/36/b1/36/36b136a2b34ec77e8d915d9225af7ddc.jpg';

  products = 
  [
    {desc:'Pots', price:12.99},
    {desc:'Dots', price:3.99},
    {desc:'Spots', price:24.99}
  ]

  flag = true;
  showFlag = false;

  // declaring a listener function binding
  handleClick()
{
    console.log(event.target)
    this.flag = false;
    this.showFlag = true;
}

handleCustomEvent(event)
{
  console.log(event)
}


}



