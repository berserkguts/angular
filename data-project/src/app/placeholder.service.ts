import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators'

@Injectable
  (
    {
      providedIn: 'root'
    }
  )

export class PlaceholderService {

  apiUrl: string = 'https://jsonplaceholder.typicode.com/'

  constructor(private http: HttpClient) {

  }

  // declare models of this service

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      return throwError('something bad happened, please try again later')

    };
  }


  getData() {
    return this.http.get(this.apiUrl).pipe(catchError(this.handleError<Object[]>('getData', []))
    );
  }

  getParamData(id, category)
  {
    return this.http.get(`${this.apiUrl}${category}/${id}`)
    .pipe(catchError(this.handleError<Object[]>('getParamData', []))
    );
  }

}
