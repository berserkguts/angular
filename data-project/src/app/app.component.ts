import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PlaceholderService } from './placeholder.service';

@Component
  (
    {
      selector: 'app-root',
      templateUrl: './app.component.html',
      styleUrls: ['./app.component.css']
    }
  )

export class AppComponent implements OnInit {


  // decalre a models for this component as an array
  users//:Object[] = []
  frmId:number = 1 
  whichCategory:string = 'users'
  choices:Object[] = [{cat:'users'},{cat:'albums'},{cat:'todos'},{cat:'posts'}];
  model

  // declare functions

  // importing service from placeholder class
  constructor(private placeholderService: PlaceholderService) {

  }

  handleClick()
  {
    this.placeholderService.getParamData(this.frmId, this.whichCategory)
    .subscribe( (result)=>{this.model = result})
  }

  // call a method from our service ie placeholder.service, calling getData method
  invokeService() {
    this.placeholderService.getData().subscribe(
      (result) => {
        console.log(result);
        this.users = result;
      })
  }


  // getData():Observable<Object[]>
  // {
  //   return this.http.get<Object[]>('https://jsonplaceholder.typicode.com/users');
  // }

  ngOnInit() {
    this.invokeService()
  }

}
